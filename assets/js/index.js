function loadData(distroName, callback) {
  const xobj = new XMLHttpRequest();
  xobj.overrideMimeType("application/json");
  xobj.open('GET', 'data/' + distroName + '.json', true);
  xobj.onreadystatechange = function () {
    if (xobj.readyState == 4 && xobj.status == "200") {
      callback(xobj.responseText);
    }
  };
  xobj.send(null);
}

function getUrlParameter(name) {
  name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
  var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
  var results = regex.exec(location.search);
  return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
}

document.addEventListener("DOMContentLoaded", () => {
  let distroQuery;
  const distro = getUrlParameter('distro') && ['ubuntu', 'debian', 'opensuse', 'arch', 'fedora'].includes(getUrlParameter('distro'));
  if (distro) {
    distroQuery = getUrlParameter('distro');
  } else {
    distroQuery = 'ubuntu';
    window.location.replace(`/?distro=ubuntu`);
  }

  selectDistro(distroQuery);
});

function selectDistro(distroQuery) {
  const screenshotElement = document.querySelector('#screenshot');
  const articleElement = document.querySelector('#article-list');
  const descElement = document.querySelector('#description-list');
  const logoElement = document.querySelector('#logo');
  const navbarElement = document.querySelector(`#${distroQuery}`);

  loadData(distroQuery, res => {
    const {distroName, descriptions, articles} = JSON.parse(res);

    // screenshot
    let screenshot = document.createElement('IMG');
    screenshot.src = `/images/${distroQuery}/screenshot-1.png`;
    screenshot.alt = `Logo ${distroName}`;
    screenshotElement.appendChild(screenshot);

    // article
    articles.forEach(val => {
      let article = document.createElement('ARTICLE');
      let title = document.createElement('H3');

      title.innerHTML = val.title;
      article.appendChild(title);

      val.text.forEach(val => {
        let paragraph = document.createElement('P');
        paragraph.innerHTML = val;
        article.appendChild(paragraph);
      });

      articleElement.appendChild(article);
    });

    // description
    descriptions.forEach(val => {
      let section = document.createElement('SECTION');
      section.innerHTML = `<h4>${val.title}</h4><p>${val.text}</p>`;
      descElement.appendChild(section);
    });

    // logo
    logoElement.innerHTML = `<img src="/images/${distroQuery}/logo.svg" alt="Logo ${distroName}">`;

    // navbar
    navbarElement.className = "active";

    // title
    document.title = `Linux Distros | ${distroName}`;
  })
}